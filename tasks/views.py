from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from tasks.models import Task
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url = reverse_lazy("home")

    def get_sucess_url(self):
        return reverse_lazy("home", args=[self.object.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(UpdateView):
    model = Task
    template_name = "tasks/update.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")

    def get_sucess_url(self):
        return reverse_lazy("show_my_tasks", args=[self.object.id])
