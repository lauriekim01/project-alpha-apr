from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(default=timezone.now)
    due_date = models.DateTimeField(default=timezone.now)
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        "projects.Project", related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name="tasks",
        on_delete=models.SET_NULL,
    )

    def __str__(self):
        return f"{self.name}"
